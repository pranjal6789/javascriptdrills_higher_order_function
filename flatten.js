function flatten(elements){
    if(!elements || elements.length==0){
        return [];
    }
    stack=[...elements];
    var final_result=[];
    while(stack.length>0){

        arr_or_value=stack.pop();

        if(Array.isArray(arr_or_value)){

            stack.push(...arr_or_value);

        }else{

            final_result.push(arr_or_value);

        }
    }

    return final_result.reverse();
}

module.exports=flatten;