function map(elements,cb){
    var arr=[];
    if(!elements || elements.length==0){
        return [];
    }
    if(typeof cb==="undefined"){
        return "undefined";
    }
    for(var i=0;i<elements.length;i++){
            arr.push(cb(elements[i],i,elements));
     }
    return arr;
}
module.exports=map;