function find(elements,cb){
    if(!elements || elements.length==0){
        return  "undefined";
    }
    if(typeof cb ==="undefined"){
        return "undefined";
    }
    for(var i=0;i<elements.length;i++){
        if(cb(elements[i])){
            return elements[i];
        }
    }
    return "undefined";
}

module.exports=find;