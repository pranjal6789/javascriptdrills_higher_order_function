function reduce(elements,cb,startingValue){
    acc=startingValue;
    if(!elements || elements.length==0){
        return "undefined";
    }
    if(typeof cb ==="undefined"){
        return "undefined";
    }
    for(var i=0;i<elements.length;i++){
        if(acc!==undefined){
            acc=cb(acc,elements[i],i,elements);
        }else{
            acc=elements[i];
        }    
    }      
    return acc;
}
module.exports=reduce;