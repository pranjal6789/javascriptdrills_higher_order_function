function filter(elements,cb){
    var arr=[];
    if(!elements || elements.length==0){
        return [];
    }
    if(typeof cb==="undefined"){
        return "undefined";
    }
    for(var i=0;i<elements.length;i++){
        if(cb(elements[i])){
            arr.push(elements[i]);
        }
    }
    if(arr.length==0){
        return [];
    }
    return arr;
}
module.exports=filter;